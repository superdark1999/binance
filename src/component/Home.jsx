import React, { useEffect, useState } from "react";
import { getInviteCode, getTotal } from "../api/home";

function Home() {
  const [inviteCode, setInviteCode] = useState("");
  const [total, setTotal] = useState([]);
  const [rewardCode, setRewardCode] = useState("");

  useEffect(async () => {
    async function fetchData() {
      const intCode = await getInviteCode();
      if (intCode.status) alert(intCode.data);
      else setInviteCode(intCode);

      const total = await getTotal();
      setTotal(total);
      console.log(total);

      // setInviteCode("hello world");
    }

    fetchData();
  }, [inviteCode]);

  const handleChangeCode = (e) => {
    setRewardCode(e.target.value);
  };
  return (
    <div
      style={{
        display: "flex",
        alignItems: "center",
        flexDirection: "column",
      }}
    >
      <input
        type="text"
        class=" form-control"
        placeholder="Reward code"
        value={rewardCode}
        onChange={handleChangeCode}
        // style={{ display: "flex", justifyContent: "center" }}
      />

      <h1 style={{ display: "flex", justifyContent: "center" }}>
        Invite: {inviteCode}
      </h1>
      {total.map((t, index) => {
        return (
          <h1 key={index} style={{ display: "flex", justifyContent: "center" }}>
            f{index}: {t}
          </h1>
        );
      })}
    </div>
  );
}

export default Home;

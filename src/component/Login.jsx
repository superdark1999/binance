import React, { useState } from "react";
import { Link } from "react-router-dom";
import { loginApi } from "../api/auth";

function Login() {
  const [user, setUser] = useState({
    username: "",
    password: "",
    inviteCode: "",
  });
  const handleLogin = async () => {
    const result = await loginApi(user);
    if (result.status === 200) {
      localStorage.setItem("auth", result.data);

      window.location = "/";
    }

    if (result.status === 400) alert(result.data);
  };

  const handleChangeUsername = (e) => {
    const cloneUser = { ...user };
    cloneUser.username = e.target.value;
    setUser(cloneUser);
  };

  const handleChangePassword = (e) => {
    const cloneUser = { ...user };
    cloneUser.password = e.target.value;
    setUser(cloneUser);
  };

  const handleChangeInviteCode = (e) => {
    const cloneUser = { ...user };
    cloneUser.inviteCode = e.target.value;
    setUser(cloneUser);
  };
  return (
    <div>
      <div>
        <div className="wrapper fadeInDown">
          <div id="formContent">
            {/* <!-- Tabs Titles --> */}

            {/* <!-- Login Form --> */}
            <form>
              <input
                type="text"
                id="login"
                className="fadeIn second"
                placeholder="Username"
                onChange={handleChangeUsername}
                value={user.username}
              />
              <input
                type="text"
                id="password"
                className="fadeIn third"
                placeholder="Password"
                value={user.password}
                onChange={handleChangePassword}
              />
              <input
                type="text"
                id="Invite code"
                className="fadeIn third"
                placeholder="Invite Code"
                value={user.inviteCode}
                onChange={handleChangeInviteCode}
              />

              <input
                onClick={handleLogin}
                type="button"
                className="fadeIn fourth"
                value="Log In"
              />
            </form>

            {/* <!-- Remind Passowrd --> */}
            <div id="formFooter">
              <Link className="underlineHover" to="/register">
                Register
              </Link>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default Login;

import React, { useState } from "react";
import { useHistory } from "react-router-dom";
import { registerApi } from "../api/auth";

function Register() {
  const history = useHistory();
  const [user, setUser] = useState({
    username: "",
    password: "",
    confirmPassword: "",
    inviteCode: "",
  });

  const handleRegister = async () => {
    const result = await registerApi(user);

    // console.log("result: ", result);
    if (result.status === 200) history.push("/login");

    if (result.status === 400) alert(result.data);

    // window.location("/");
  };

  const handleChangeUsername = (e) => {
    const cloneUser = { ...user };
    cloneUser.username = e.target.value;
    setUser(cloneUser);
  };

  const handleChangePassword = (e) => {
    const cloneUser = { ...user };
    cloneUser.password = e.target.value;
    setUser(cloneUser);
  };
  const handleChangeConfirmPassword = (e) => {
    const cloneUser = { ...user };
    cloneUser.confirmPassword = e.target.value;
    setUser(cloneUser);
  };

  const handleChangeInviteCode = (e) => {
    const cloneUser = { ...user };
    cloneUser.inviteCode = e.target.value;
    setUser(cloneUser);
  };

  return (
    <div>
      <div>
        <div className="wrapper fadeInDown">
          <div id="formContent">
            {/* <!-- Tabs Titles --> */}

            {/* <!-- Login Form --> */}
            <form>
              <input
                type="text"
                id="login"
                className="fadeIn second"
                placeholder="Username"
                onChange={handleChangeUsername}
                value={user.username}
              />
              <input
                type="text"
                id="password"
                className="fadeIn third"
                placeholder="Password"
                value={user.password}
                onChange={handleChangePassword}
              />
              <input
                type="text"
                id="inviteCode"
                className="fadeIn third"
                placeholder="Confirm Password"
                value={user.confirmPassword}
                onChange={handleChangeConfirmPassword}
              />
              <input
                type="text"
                id="inviteCode"
                className="fadeIn third"
                placeholder="Invite Code"
                value={user.inviteCode}
                onChange={handleChangeInviteCode}
              />
              <input
                onClick={handleRegister}
                type="button"
                className="fadeIn fourth"
                value="Register"
              />
            </form>
          </div>
        </div>
      </div>
    </div>
  );
}

export default Register;

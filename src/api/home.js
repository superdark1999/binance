import axios from "axios";

const url = "http://localhost:5000";

export const getInviteCode = async () => {
  const token = localStorage.getItem("auth");

  try {
    const invite = await axios.get(`${url}/invitecode`, {
      headers: { auth: token },
    });

    return invite.data;
  } catch (error) {
    return error.response;
  }
};

export const getTotal = async () => {
  const token = localStorage.getItem("auth");

  const total = await axios.get(`${url}/profile`, {
    headers: { auth: token },
  });

  return total.data;
};

export {};

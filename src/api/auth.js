import axios from "axios";

const url = "http://localhost:5000/auth";

export const loginApi = async (user) => {
  try {
    const result = await axios.post(`${url}/login`, {
      email: user.username,
      password: user.password,
      inviteCode: user.inviteCode,
    });
    return result;
  } catch (error) {
    if (error.response) {
      /*
       * The request was made and the server responded with a
       * status code that falls out of the range of 2xx
       */

      return error.response;
    }
  }
};

export const registerApi = async ({
  username,
  password,
  confirmPassword,
  inviteCode,
}) => {
  try {
    const result = await axios.post(`${url}/register`, {
      email: username,
      password: password,
      confirmPassword: confirmPassword,
      inviteCode: inviteCode,
    });

    return result;
  } catch (error) {
    if (error.response) {
      /*
       * The request was made and the server responded with a
       * status code that falls out of the range of 2xx
       */

      return error.response;
    }
  }
};
